﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI
{
    class SumDTO
    {
        public string Id { get; set; }

        public decimal Sum { get; set; }
    }
}
