﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Linq;
using CoolParking.BL.Models;
using System.Collections.Generic;
using System.Text.Json;

namespace UI
{
    class Program
    {
        public const string path = "https://localhost:5001/api/";

        public static HttpClient client = new HttpClient();
        static void Main(string[] args)
        {
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("-------------Welcome to Cool-Parking--------------");
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine();
            Console.WriteLine(" This is console interface. You can use command ");
            Console.WriteLine(" \"man\" to see list of available commands. And ");
            Console.WriteLine(" \"quit\" to exit from program ");
            Console.WriteLine();

            bool flag = true;
            while (flag)
            {
                Console.Write(">");
                string command = Console.ReadLine();
                Console.WriteLine();
                switch (command)
                {
                    case "quit":
                        flag = false;
                        break;
                    case "man":
                        Man();
                        break;
                    case "balance":
                        Balance();
                        break;
                    case "sum":
                        Sum();
                        break;
                    case "free":
                        Free();
                        break;
                    case "curntr":
                        CurrentTransactions();
                        break;
                    case "history":
                        History();
                        break;
                    case "vehicles":
                        AllVehicles();
                        break;
                    case "put":
                        Put();
                        break;
                    case "take":
                        Take();
                        break;
                    case "pay":
                        Replenish();
                        break;
                    default:
                        Console.WriteLine("What? =/");
                        break;
                }
                Console.WriteLine();
            }
        }

        static void Man()
        {
            Console.WriteLine("quit - exit from program");
            Console.WriteLine("balance - balance of parking");
            Console.WriteLine("sum - sum of current earnings");
            Console.WriteLine("free - free/busy places on parking");
            Console.WriteLine("curntr - current transactions");
            Console.WriteLine("history - history of transactions");
            Console.WriteLine("vehicles - list of all vehicles on parking");
            Console.WriteLine("put - put vehicle on parking");
            Console.WriteLine("take - take vehicle from parking");
            Console.WriteLine("pay - replenish vehicle balance");
        }

        static void Balance()
        {
            decimal balance;
            balance = client.GetFromJsonAsync<decimal>(path + "Parking/balance").Result;
            Console.WriteLine("Balance of parking: {0}", balance);
        }

        static void Sum()
        {
            decimal sum = 0;
            TransactionInfo[] tr = client.GetFromJsonAsync<TransactionInfo[]>(path + "Transactions/last").Result;
            sum = tr.Sum(t => t.Sum);
            Console.WriteLine("Sum of current earnings: {0}", sum);
        }

        static void Free()
        {
            int free, capacity;
            capacity = client.GetFromJsonAsync<int>(path + "Parking/capacity").Result;
            free = client.GetFromJsonAsync<int>(path + "Parking/freePlaces").Result;
            Console.WriteLine("Free {0} places from {1} (busy {2})", free, capacity, capacity - free);
        }

        static void CurrentTransactions()
        {
            TransactionInfo[] tr;
            tr = client.GetFromJsonAsync<TransactionInfo[]>(path + "Transactions/last").Result;
            foreach (TransactionInfo s in tr)
            {
                Console.WriteLine($" {s.Time} | ID: {s.Id} | Sum: {s.Sum}");
            }
        }

        static void History()
        {
            try
            {
                string tr = client.GetStringAsync(path + "Transactions/all").Result;
                Console.WriteLine(tr);
            }
            catch (Exception)
            {
                Console.WriteLine("Log does not exist");
            }
        }

        static void AllVehicles()
        {
            var responce = client.GetAsync(path + "Vehicles").Result.Content.ReadAsStringAsync().Result;
            var vehicles = JsonSerializer.Deserialize<List<VehicleDTO>>(responce);
            foreach (var v in vehicles)
            {
                Console.WriteLine($" ID: {v.Id} | Type: {v.VehicleType} | Balance: {v.Balance}");
            }
        }

        static void Put()
        {
            try
            {
                // Get ID
                Console.WriteLine("First you have to say what you want to put");
                Console.WriteLine();
                Console.WriteLine("Enter ID (For example AA-1234-BB)");
                Console.WriteLine();
                Console.Write("ID: ");
                string id = Console.ReadLine();
                // Get Type
                Console.WriteLine();
                Console.WriteLine("Enter number to set vehicle type:");
                Console.WriteLine("1 - car  2 - bus  3 - truck  4 - motorcycle ");
                Console.Write("Type: ");
                int type = Convert.ToInt32(Console.ReadLine());
                if(type < 1 || type > 4)
                {
                    throw new Exception();
                }
                // Get Balance
                Console.WriteLine();
                Console.WriteLine("Enter bigin vehicle balance");
                Console.Write("Balance: ");
                decimal balance = Convert.ToDecimal(Console.ReadLine());
                // Creating Vehicle
                Console.WriteLine();
                var vehicle = new VehicleDTO() { Id = id, Balance = balance, VehicleType = (VehicleType)type };
                var result = client.PostAsJsonAsync<VehicleDTO>(path + "Vehicles", vehicle).Result;
                if (result.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    throw new Exception();
                }
                Console.WriteLine("OK");
            }
            catch (Exception)
            {
                Console.WriteLine();
                Console.WriteLine("Dude, you entered something incorrectly!");
                Console.WriteLine("Try Again!");
            }
        }

        static void Take()
        {
            Console.WriteLine("Enter vehicle ID wich you wanna take");
            Console.WriteLine();
            Console.Write("ID: ");
            try
            {
                string id = Console.ReadLine();
                var result = client.DeleteAsync(path + "Vehicles/" + id).Result;
                if (result.StatusCode == System.Net.HttpStatusCode.BadRequest ||
                    result.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    throw new Exception();
                }
                Console.WriteLine("OK");
            }
            catch (Exception)
            {
                Console.WriteLine();
                Console.WriteLine("Dude, you entered something incorrectly!");
                Console.WriteLine("Try Again!");
            }
        }

        static void Replenish()
        {
            Console.WriteLine("Enter vehicle ID its balance you wanna replenish");
            Console.WriteLine();
            Console.Write("ID: ");
            try
            {
                string id = Console.ReadLine();
                Console.WriteLine();
                Console.Write("Sum: ");
                decimal sum = Convert.ToDecimal(Console.ReadLine());
                SumDTO sumDTO = new SumDTO() { Id = id, Sum = sum };
                var result = client.PutAsJsonAsync<SumDTO>(path + "Transactions/topUpVehicle", sumDTO).Result;
                if (result.StatusCode == System.Net.HttpStatusCode.BadRequest ||
                result.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    throw new Exception();
                }
                Console.WriteLine("OK");
            }
            catch (Exception)
            {
                Console.WriteLine();
                Console.WriteLine("Dude, you entered something incorrectly!");
                Console.WriteLine("Try Again!");
            }
        }
    }
}

