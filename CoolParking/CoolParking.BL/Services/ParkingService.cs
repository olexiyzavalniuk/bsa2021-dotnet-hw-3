﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;
using System.Timers;
using System;
using System.Linq;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking _parking;

        internal ILogService _logService;

        private ITimerService _withdrawTimer;

        private ITimerService _logTimer;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.GetInstance();
            _logService = logService;
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Elapsed += _parking.GetPayment;
            _logTimer = logTimer;
            _logTimer.Elapsed += WriteLog;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            _parking.AddVehicle(vehicle);
        }

        public void Dispose()
        {
            _parking.vehicles.Clear();
            _parking.Balance = Settings.Balance;
            _parking.TransactionInfo.Clear();
            _logService = null;
            _withdrawTimer.Dispose();
            _withdrawTimer = null;
            _logTimer.Dispose();
            _logTimer = null;
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.GetFreePlaces();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _parking.TransactionInfo.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parking.GetVehicles();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            _parking.RemoveVehicle(vehicleId);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            _parking.TopUpVehicle(vehicleId, sum);
        }

        public void WriteLog(object source, ElapsedEventArgs e)
        {
            string str;
            _logService.Write("============================================");
            foreach (TransactionInfo transactionInfo in _parking.TransactionInfo)
            {
                str = transactionInfo.Time + " | Id: " + transactionInfo.Id + " | Sum: " + transactionInfo.Sum;
                _logService.Write(str);
            }
            _parking.TransactionInfo.Clear();
        }

        public string[] LogForUI()
        {
            string[] transactions = new string[_parking.TransactionInfo.Count];
            int i = 0;
            foreach (TransactionInfo transactionInfo in _parking.TransactionInfo)
            {
                transactions[i] = transactionInfo.Time + " | Id: " + transactionInfo.Id + " | Sum: " + transactionInfo.Sum;
                i++;
            }
            return transactions;
        }

        public decimal Sum()
        {
            decimal sum = 0;
            foreach (TransactionInfo transactionInfo in _parking.TransactionInfo)
            {
                sum += transactionInfo.Sum;
            }
            return sum;
        }

        public string[] AllVihicles()
        {
            string[] v = new string[_parking.vehicles.Count];
            int i = 0;
            foreach (Vehicle vehicle in _parking.vehicles)
            {
                v[i] = " ID: " + vehicle.Id + "  type: " + vehicle.VehicleType + "  balance: " + vehicle.Balance; 
                i++;
            }
            return v;
        }

        public Vehicle GetVehicle(string vehicleId)
        {
            if(!Vehicle.IsIdValid(vehicleId))
            {
                throw new ArgumentException("Id not valid");
            }            
            Vehicle vehicle = _parking.vehicles.FirstOrDefault(vehicle => vehicle.Id == vehicleId);
            if(vehicle == null)
            {
                throw new InvalidOperationException("Vehicle not found");
            }

            return vehicle;
        }
    }
}