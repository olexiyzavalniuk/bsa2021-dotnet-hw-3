﻿namespace CoolParking.WebAPI.Models
{
    public class TopUpVehicle
    {
        public string Id { get; set; }

        public decimal Sum { get; set; }
    }
}
