﻿using CoolParking.BL.Models;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.Models
{
    public class AddVehicle
    {
        public string Id { get; set; }

        public VehicleType VehicleType { get; set; }

        public decimal Balance { get;  set; }
    }
}
