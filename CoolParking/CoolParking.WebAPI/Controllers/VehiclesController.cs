﻿using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public ActionResult<List<Vehicle>> GetVehicles()
        {
                return Ok(_parkingService.GetVehicles());
        }

        [Route("{id}")]
        [HttpGet]
        public ActionResult<Vehicle> GetVehicle(string id)
        {
            try
            {
                Vehicle vehicle = _parkingService.GetVehicle(id);
                return Ok(vehicle);
            }
            catch(ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch(InvalidOperationException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        public ActionResult<Vehicle> AddVehicle([FromBody] AddVehicle addVehicle)
        {
            try
            {
                Vehicle vehicle = new Vehicle(addVehicle.Id, addVehicle.VehicleType, addVehicle.Balance);
                _parkingService.AddVehicle(vehicle);
                return Created("/api/vehicles/post", vehicle);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("{id}")]
        [HttpDelete]
        public ActionResult DeleteVehicle(string id)
        {
            if (!Vehicle.IsIdValid(id))
            {
                return BadRequest("Id not valid");
            }
            try
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch(ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
