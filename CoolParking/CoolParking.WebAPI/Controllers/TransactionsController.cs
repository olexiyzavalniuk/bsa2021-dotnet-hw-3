﻿using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [Route("last")]
        [HttpGet]
        public ActionResult<TransactionInfo[]> GetLastTransactions()
        {
            return Ok(_parkingService.GetLastParkingTransactions());
        }

        [Route("all")]
        [HttpGet]
        public ActionResult<string> GetLog()
        {
            return Ok(_parkingService.ReadFromLog());
        }

        [Route("topUpVehicle")]
        [HttpPut]
        public ActionResult<Vehicle> Pay([FromBody] TopUpVehicle topUpVehicle)
        {
            if (!Vehicle.IsIdValid(topUpVehicle.Id))
            {
                return BadRequest("Id not valid");
            }
            try
            {
                _parkingService.TopUpVehicle(topUpVehicle.Id, topUpVehicle.Sum);
                return Ok(_parkingService.GetVehicle(topUpVehicle.Id));
            }
            catch (ArgumentException e)
            {
                if (e.Message == "Vehicle not found")
                {
                    return NotFound(e.Message);
                }
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }
    }
}
